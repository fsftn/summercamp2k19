# Summer Camp 2k19

Onepage site for the event

# Sections 

## Why Python?
## Workshop Package

- Lunch for two days
- Tea & Biscuit twice a day for two days
- Certificate
- ID Tag
- FSHM Annual Membership
- Workshop Booklet

## Workshop Checklist for Participants

- College / School / Govt ID Card
- Laptop 
- Min. 2GB pendrive
- Notebook
- Pen / Pencil

## Event Schedule



## Map & Bus Instructions

- Bus - Lawspet to New Bus Stand Route
- Bus - Ganapathichettikulam to New Bus Stand
- Tempo - Raja Theatre to Muruga  / JIPMER / Ousteri

## About US

### GLUGs
- PU 
- SMVEC
- PuduvaiGLUG

### Past Events 
- Signature Campaing - Jan 2019
- Hacktoberfest - Oct 2019
- Free Software & Hardware Expo - Sep 2019
- March For Science - April 2019
- Software Freedom Day - Oct 2017
